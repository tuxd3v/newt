# newt
---

Patches for newt packages

#### Table of contents
* [Introduction](#introduction)
* [Characteristics](#characteristics)
* [Archs](#archs)
* [Testing](#testing)
* [Credits](#credits)

### Introduction:
----
This pacthes will be, automatically, applied when you build several packages from it( from sources.. ).
If you want to apply patches by hand:
   ```lua
   # jump to newt directory
   cd newt
   patch -p1 < debian/patches/the_patch_you_want_to_apply.patch
   ```
The newt source code should be patched automatically..

### Characteristics:
----
* Add Escape HotKey to forms.c, to maintain previous behaviour, this will also add this functionality to tools like **whiptail**, and the **libraries**, and such..

### Archs:
----
This paches are Architecture independent, they should be built for any arch were you want support for the newt libraries or tools..

### Testing:
----
Tests were done, and everything is working again, I tested with **lnewt**, the behaviour is restored..

### Credits
----
patches       : tuxd3v

